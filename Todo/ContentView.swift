//
//  ContentView.swift
//  Todo
//
//  Created by Pinakpani Mukherjee on 7/30/20.
//  Copyright © 2020 Pinakpani Mukherjee. All rights reserved.
//

import SwiftUI

struct ContentView: View {
   @State var todo: String = ""
    @State var todoList =  [String]()
    @State var timeAgo : String = ""
    
    var body: some View {
        NavigationView(){
            VStack{
                HStack(spacing:5){
                    Image(systemName: "plus.circle")
                        .padding(.leading)
                    Group{
                        TextField("What do you want to do today?",text : self.$todo, onEditingChanged: { (changed) in
                            print(changed)
                        }) {
                            self.timeAgo = self.timeAgoSinceDate(Date())
                            self.todoList.insert(self.todo, at: 0)
                            print("added item : \(self.todoList)")
                            self.todo = ""//clear field
                            
                        }.padding(.all,12)
                        
                        }.background(Color.green)
                        .clipShape(RoundedRectangle(cornerRadius: 5))
                    .shadow(radius: 5)
                        .padding(.trailing,8)
                    
                }
                List {
                ForEach(self.todoList, id: \.self) { item in //Beta 4 update!
                    ToDoRow(toDoItem: item, timeAgo: self.timeAgo)
                  
                    }.onDelete(perform: deleteItem)                }
                
            }.navigationBarTitle(Text("To Do"))
            
        }
     }
    
    func deleteItem(at offsets: IndexSet){
        guard let index = Array(offsets).first else {
            return
        }
        print("Removed : \(self.todoList[index])")
        
        self.todoList.remove(at: index)
    }

    func timeAgoSinceDate(_ date:Date, numericDates:Bool = false) -> String {
        
        //source: https://gist.github.com/minorbug/468790060810e0d29545
        
        let calendar = NSCalendar.current
        let unitFlags: Set<Calendar.Component> = [.minute, .hour, .day, .weekOfYear, .month, .year, .second]
        let now = Date()
        let earliest = now < date ? now : date
        let latest = (earliest == now) ? date : now
        let components = calendar.dateComponents(unitFlags, from: earliest,  to: latest)
        
        if (components.year! >= 2) {
            return "\(components.year!) years ago"
        } else if (components.year! >= 1){
            if (numericDates){
                return "1 year ago"
            } else {
                return "Last year"
            }
        } else if (components.month! >= 2) {
            return "\(components.month!) months ago"
        } else if (components.month! >= 1){
            if (numericDates){
                return "1 month ago"
            } else {
                return "Last month"
            }
        } else if (components.weekOfYear! >= 2) {
            return "\(components.weekOfYear!) weeks ago"
        } else if (components.weekOfYear! >= 1){
            if (numericDates){
                return "1 week ago"
            } else {
                return "Last week"
            }
        } else if (components.day! >= 2) {
            return "\(components.day!) days ago"
        } else if (components.day! >= 1){
            if (numericDates){
                return "1 day ago"
            } else {
                return "Yesterday"
            }
        } else if (components.hour! >= 2) {
            return "\(components.hour!) hours ago"
        } else if (components.hour! >= 1){
            if (numericDates){
                return "1 hour ago"
            } else {
                return "An hour ago"
            }
        } else if (components.minute! >= 2) {
            return "\(components.minute!) minutes ago"
        } else if (components.minute! >= 1){
            if (numericDates){
                return "1 minute ago"
            } else {
                return "A minute ago"
            }
        } else if (components.second! >= 3) {
            return "\(components.second!) seconds ago"
        } else {
            return "Just now"
        }
        
    }

}

struct ToDoRow: View{
    @State var toDoItem : String = ""
    @State var isDone : Bool = false
    @State var timeAgo : String

    var body : some View{
        VStack(alignment: .center
            , spacing: 2){
                Group{
                    HStack{
                        Text(toDoItem)
                            .foregroundColor(.white)
                            .multilineTextAlignment(.leading)
                        .lineLimit(2)
                        Spacer()
                        Image(systemName: (self.isDone) ? "checkmark":"square")
                        .padding()
                    }
                    HStack(alignment: .center, spacing: 3){
                        Spacer()
                        Text("Added: \(self.timeAgo)")
                            .foregroundColor(.white)
                        .italic()
                            .padding(.all,4)
                    }.padding(.bottom,5)
                }.padding(.all,4)
        
        }.opacity((self.isDone) ? 0.3:1)
        .background((self.isDone) ? Color.gray : Color.red)
        .clipShape(RoundedRectangle(cornerRadius: 5))
            .onTapGesture {
                print("tapped")
                self.isDone.toggle()
                print("tapped! Is done \(self.isDone)")
        }
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
